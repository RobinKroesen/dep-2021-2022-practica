{-|
    Module      : Lib
    Description : Checkpoint voor V2DEP: recursie en lijsten
    Copyright   : (c) Brian van de Bijl, 2020
    License     : BSD3
    Maintainer  : nick.roumimper@hu.nl

    In dit practicum oefenen we met het schrijven van simpele functies in Haskell.
    Specifiek leren we hoe je recursie en pattern matching kunt gebruiken om een functie op te bouwen.
    LET OP: Hoewel al deze functies makkelijker kunnen worden geschreven met hogere-orde functies,
    is het hier nog niet de bedoeling om die te gebruiken.
    Hogere-orde functies behandelen we verderop in het vak; voor alle volgende practica mag je deze
    wel gebruiken.
    In het onderstaande commentaar betekent het symbool ~> "geeft resultaat terug";
    bijvoorbeeld, 3 + 2 ~> 5 betekent "het uitvoeren van 3 + 2 geeft het resultaat 5 terug".
-}

module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where


-- TODO: Schrijf en documenteer de functie ex1, die de som van een lijst getallen berekent.
-- Voorbeeld: ex1 [3,1,4,1,5] ~> 14
-- | ex1 pakt het eerste item van een lijst(x) telt het op bij het eerste item van de lijst zonder het eerste teken, dit herhaald zich totdat er geen lijst meer over is(recursie).
-- | ex1 returned de som van de lijst.
ex1 :: [Int] -> Int
ex1 [] = 0
ex1 (x:xs) = x + ex1 xs


-- TODO: Schrijf en documenteer de functie ex2, die alle elementen van een lijst met 1 ophoogt.
-- Voorbeeld: ex2 [3,1,4,1,5] ~> [4,2,5,2,6]
-- | ex2 returned een lijst van een gegeven lijst met elk getal plus 1. 
-- | Dit doet ex2 door een lijst aan te maken met het eerste element van een lijst(x) plus 1 en de volgende elementen krijg je door de functie opnieuw aan te roepen met de overgebleven lijst(xs)(recursie).  
ex2 :: [Int] -> [Int]
ex2 [] = []
ex2 (x:xs) = x + 1 : ex2 xs

-- TODO: Schrijf en documenteer de functie ex3, die alle elementen van een lijst met -1 vermenigvuldigt.
-- Voorbeeld: ex3 [3,1,4,1,5] ~> [-3,-1,-4,-1,-5]
-- | ex3 heeft als input een lijst en als output een lijst met alle elementen vermenigvuldigd met -1.
-- | Dit doet ex3 loopt op dezelfde manier door de elementen als ex2 maar i.p.v. 1 bij de elementen op te tellen worden de elementen met -1 vermenigvuldigd.
-- | belangrijk dat er haakjes om de -1 staan omdat anders de elementen met - worden vermenigvuldigd en dat geeft een error. 
ex3 :: [Int] -> [Int]
ex3 [] = []
ex3 (x:xs) = x * (-1) : ex3 xs 

-- TODO: Schrijf en documenteer de functie ex4, die twee lijsten aan elkaar plakt.
-- Voorbeeld: ex4 [3,1,4] [1,5] ~> [3,1,4,1,5]
-- Maak hierbij geen gebruik van de standaard-functies, maar los het probleem zelf met (expliciete) recursie op. 
-- Hint: je hoeft maar door een van beide lijsten heen te lopen met recursie.
-- | ex4 heeft als input 2 lijsten waarbij en als output 1 lijst met alle elementen van beide lijsten
-- | Dit doet ex4 door de eerste lijst recursief te doorlopen en steeds het element toe te voegen aan de 2e lijst. 
ex4 :: [Int] -> [Int] -> [Int]
ex4 [] y = y
ex4 (x:xs) y = x : ex4 xs y 

-- TODO: Schrijf en documenteer een functie, ex5, die twee lijsten van gelijke lengte paarsgewijs bij elkaar optelt.
-- Voorbeeld: ex5 [3,1,4] [1,5,9] ~> [4,6,13]
-- | De functie heeft als input 2 lijsten en heeft als output 1 lijst met de elementen paarsgewijs opgeteld.
-- | Dit doet de functie door de eerste elementen van de lijsten bij elkaar op te tellen en dan recursief de volgende elementen bij elkaar op tellen.
ex5 :: [Int] -> [Int] -> [Int]
ex5 [] [] = []
ex5 (x:xs) (y:ys) = x+y : ex5 xs ys

-- TODO: Schrijf en documenteer een functie, ex6, die twee lijsten van gelijke lengte paarsgewijs met elkaar vermenigvuldigt.
-- Voorbeeld: ex6 [3,1,4] [1,5,9] ~> [3,5,36] 
-- | ex6 heeft als input 2 lijsten en als output 1 lijst waar de elementen paarsgewijs vermenigvuldigd zijn.
-- | Dit doet de functie door de eerste elementen van de lijsten te vermenigvuldigen en dan recursief de volgende elementen vermenigvuldigen.
ex6 :: [Int] -> [Int] -> [Int]
ex6 [] [] = []
ex6 (x:xs) (y:ys) = x*y : ex6 xs ys

-- TODO: Schrijf en documenteer een functie, ex7, die de functies ex1 en ex6 combineert tot een functie die het inwendig product uitrekent.
-- Voorbeeld: ex7 [3,1,4] [1,5,9] geeft 3*1 + 1*5 + 4*9 = 44 terug als resultaat.
-- | ex7 heeft als input 2 lijsten en als output 1 lijst waarbij de elementen ten eerste paarsgewijs vermenigvuldigd zijn en daarna bij elkaar opgeteld zijn.
-- | Dit doet de functie door eerste ex6 te gebruiken die de 2 lijsten paarsgewijs vermenigvuldigd en over het resultaat van ex6, ex1 uitvoeren die de elementen van de lijst bij elkaar opteld.
ex7 :: [Int] -> [Int] -> Int
ex7 [] [] = 0
ex7 (x:xs) (y:ys) = ex1 (ex6 (x:xs) (y:ys))